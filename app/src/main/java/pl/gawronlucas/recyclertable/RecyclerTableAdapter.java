package pl.gawronlucas.recyclertable;

import android.graphics.Color;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Łukasz Gawron (gawronlucas@gmail.com) on 2015-09-17.
 */
public class RecyclerTableAdapter extends RecyclerView.Adapter<RecyclerTableAdapter.TableItemViewHolder> {

    private List<TableItem> items;
    private List<TableItem> columnHeader;
    private List<TableItem> rowHeader;

    private static final int COLUMN_HEADER_TYPE = 1;
    private static final int ROW_HEADER_TYPE = 2;
    private static final int ITEM_TYPE = 3;

    public RecyclerTableAdapter(List<TableItem> items) {
        this.setItems(items);
    }

    public RecyclerTableAdapter setColumnHeader(List<TableItem> header) {
        this.columnHeader = header;
        return this;
    }

    public RecyclerTableAdapter setRowHeader(List<TableItem> header) {
        this.rowHeader = header;
        return this;
    }

    @Override
    public TableItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_table_item, null);

        return new TableItemViewHolder(view, viewType);
    }

    @Override
    public int getItemCount() {
        int itemsSize = items.size();
        int columnHeaderSize = columnHeader != null ? columnHeader.size() : 0;
        int rowHeaderSize = rowHeader != null ? rowHeader.size() : 0;
        return itemsSize + columnHeaderSize + rowHeaderSize;
    }

    @Override
    public void onBindViewHolder(TableItemViewHolder tableItemViewHolder, int position) {
        tableItemViewHolder.view.setText("Item " + position + " : type: " + tableItemViewHolder.viewType);
        if(tableItemViewHolder.viewType == COLUMN_HEADER_TYPE) {
            tableItemViewHolder.view.setBackgroundColor(Color.BLUE);
            tableItemViewHolder.view.setTextColor(Color.WHITE);
            tableItemViewHolder.view.setPadding(
                    tableItemViewHolder.view.getPaddingLeft(),
                    tableItemViewHolder.view.getPaddingTop() + 4,
                    tableItemViewHolder.view.getPaddingRight(),
                    tableItemViewHolder.view.getPaddingBottom() + 4);
        } else if (tableItemViewHolder.viewType == ITEM_TYPE) {
            tableItemViewHolder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popupMenu = new PopupMenu(v.getContext(), v);
                    popupMenu.inflate(R.menu.menu_main);
                    popupMenu.show();
                }
            });
        } else if (tableItemViewHolder.viewType == ROW_HEADER_TYPE) {
            tableItemViewHolder.view.setBackgroundColor(Color.CYAN);
            tableItemViewHolder.view.setTextColor(Color.WHITE);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (columnHeader != null && position >= 0 && position < columnHeader.size()) {
            return COLUMN_HEADER_TYPE;
        } else if (position != 0 && position % columnHeader.size() == 0) {
            return ROW_HEADER_TYPE;
        } else {
            return ITEM_TYPE;
        }
    }

    public List<TableItem> getItems() {
        return items;
    }

    public void setItems(List<TableItem> items) {
        this.items = items;
    }

    class TableItemViewHolder extends RecyclerView.ViewHolder {
        TextView view;
        int viewType;
        public TableItemViewHolder(View itemView, int viewType) {
            super(itemView);
            view = (TextView) itemView.findViewById(R.id.recycler_table_item_tv);
            this.viewType = viewType;
        }
    }

    public static class TableItem {
        public TableItem() {

        }
    }

}
