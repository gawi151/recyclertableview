package pl.gawronlucas.recyclertable;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView view;
    RecyclerTableAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        view = (RecyclerView) findViewById(R.id.recycler_table);

        List<RecyclerTableAdapter.TableItem> list = new ArrayList<>();
        for (int i = 0; i < 120; i++) {
            RecyclerTableAdapter.TableItem item = new RecyclerTableAdapter.TableItem();
            list.add(item);
        }
        List<RecyclerTableAdapter.TableItem> header = new ArrayList<>(3);
        for (int i = 0; i < 3; i++) {
            RecyclerTableAdapter.TableItem item = new RecyclerTableAdapter.TableItem();
            header.add(item);
        }

        adapter = new RecyclerTableAdapter(list);
        adapter.setColumnHeader(header);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, header.size(), GridLayoutManager.VERTICAL, false);
        view.setAdapter(adapter);
        view.setLayoutManager(gridLayoutManager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
